# NGINX Redirect

This is a simple NGINX configuration to redirect all non SSL traffic
to HTTPS.

The `Dockerfile` looks like:

```
FROM nginx:stable-alpine
COPY nginx.conf /etc/nginx/nginx.conf
```

and the relevant portion of `nginx.conf` is:

```
# redirect all traffic to ssl
server {
      listen         80;
      server_name    *.hspconsortium.org;
      if ($http_x_forwarded_proto != "https") {
          rewrite ^(.*)$ https://$server_name$REQUEST_URI permanent;
      }
}
```
