#!/usr/bin/env bash

export PROJECT_NAME="nginx-redirect"

export PROJECT_FULL_NAME="${PROJECT_NAME}-${TARGET_ENV}"

export PROJECT_VERSION="0.3.0"

export DOCKER_IMAGE_COORDINATES="hspconsortium/${PROJECT_NAME}:${PROJECT_VERSION}"

export SPRING_PROFILES_ACTIVE=""

export TEMPLATE_FILE="../aws/task-definition.json"
